<?php
/**
 * Author: Sachit Tandukar, Dipika Shrestha
 * Email: linkin_sachit@hotmail.com
 * Website: http://twitter.com/sachitandukar
 * License: DBAD license. You can find more detail about license in this link: http://www.dbad-license.org/
 * File: Validation.php
 */
class Validation
{
    public $errors = array();
    protected $_error_prefix = '<p>';
    protected $_error_suffix = '</p>';

    public function validate($data, $rules)
    {
        $valid = true;
        foreach ($rules as $fieldname => $rule) {
            $callbacks = explode('|', $rule);
            foreach ($callbacks as $callback) {
                $value = isset($data[$fieldname]) ? $data[$fieldname] : null;
                $new_callback = explode(':', $callback);
                if (count($new_callback) > 1):
                    if ($this->$new_callback[0]($value, $fieldname, $new_callback[1]) == false): $valid = false; endif;
                else:
                    if ($this->$new_callback[0]($value, $fieldname) == false): $valid = false; endif;
                endif;
            }
        }
        return $valid;
    }

    public function set_error_delimiters($prefix = '<p>', $suffix = '</p>')
    {
        $this->_error_prefix = $prefix;
        $this->_error_suffix = $suffix;

        return $this;
    }

    public function validation_error($prefix = '', $suffix = '')
    {
        if ($prefix == '') {
            $prefix = $this->_error_prefix;
        }

        if ($suffix == '') {
            $suffix = $this->_error_suffix;
        }
        $message = '';
        foreach ($this->errors as $error) {
            $message .= $prefix . $error . $suffix;
        }
        return $message;
    }

    /**
     * returns false if field is not valid E-mail
     * @param $value
     * @param $fieldname
     * @return mixed
     */
    public function email($value, $fieldname)
    {
        $valid = filter_var($value, FILTER_VALIDATE_EMAIL);
        if ($valid == false) $this->errors[] = "The " . ucwords($fieldname) . " field needs to be a valid E-mail address";
        return $valid;
    }

    /**
     * returns false if field is empty
     * @param $value
     * @param $fieldname
     * @return bool
     */
    public function required($value, $fieldname)
    {
        $valid = !empty($value);
        if ($valid == false) $this->errors[] = "The " . ucwords($fieldname) . " field is required";
        return $valid;
    }

    /**
     * returns false if field value contains other character than alphabets
     * @param $value
     * @param $fieldname
     * @return bool
     */
    public function alpha($value, $fieldname)
    {
        $valid = ctype_alpha($value);
        if ($valid == false) $this->errors[] = "The " . ucwords($fieldname) . " field only accepts alphabets character";
        return $valid;
    }

    /**
     * returns false if field value contains other character than alphabets and numeric
     * @param $value
     * @param $fieldname
     * @return bool
     */
    public function alpha_numeric($value, $fieldname)
    {
        $valid = ctype_alnum($value);
        if ($valid == false) $this->errors[] = "The " . ucwords($fieldname) . " field only accepts alphabets and numeric character";
        return $valid;
    }

    /**
     * returns false if field value contains other character than alphabets, dash, underscores and numeric
     * @param $value
     * @param $fieldname
     * @return bool
     */
    public function alpha_dash($value, $fieldname)
    {
        $valid = preg_match("/^([-a-z0-9_-])+$/i", $value);
        if ($valid == false) $this->errors[] = "The " . ucwords($fieldname) . " field only accepts alphabets, dash, underscores and numeric character";
        return $valid;
    }

    /**
     * returns false if field value contains other character than numeric
     * @param $value
     * @param $fieldname
     * @return bool
     */
    public function numeric($value, $fieldname)
    {
        $valid = ctype_digit($value);
        if ($valid == false) $this->errors[] = "The " . ucwords($fieldname) . " field only accepts numeric character";
        return $valid;
    }

    /**
     * returns false if field value does not contain lowercase character
     * @param $value
     * @param $fieldname
     * @return bool
     */
    public function lowercase($value, $fieldname)
    {
        $valid = ctype_lower($value);
        if ($valid == false) $this->errors[] = "The " . ucwords($fieldname) . " field does not contain all lowercase letters";
        return $valid;
    }

    /**
     * returns false if field value does not contain uppercase character
     * @param $value
     * @param $fieldname
     * @return bool
     */
    public function uppercase($value, $fieldname)
    {
        $valid = ctype_upper($value);
        if ($valid == false) $this->errors[] = "The " . ucwords($fieldname) . " field does not contain all uppercase letters";
        return $valid;
    }

    /**
     * returns false if field value does not contain strong password
     * @param $value
     * @param $fieldname
     * @return bool
     */
    public function strong_password($value, $fieldname)
    {
        $valid = preg_match("#.*^(?=.{8,16})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#", $value);
        if ($valid == false) $this->errors[] = "The " . ucwords($fieldname) . " field is too weak for password. Password should be 8 to 16 character string in length with at least one upper case letter, one lower case letter and one digit";
        return $valid;
    }

    /**
     * returns false if value of field does not match with value of other field
     * @param $value
     * @param $fieldname
     * @param $attributes
     * @return bool
     */
    public function matches($value, $fieldname, $attributes)
    {
        $field = $_POST[$attributes];
        $valid = ($value !== $field) ? false : true;
        if ($valid == false) $this->errors[] = "The " . ucwords($fieldname) . " field does not match with " . ucwords($attributes) . " field";
        return $valid;
    }

    /**
     * returns false if value of field is not of minimum defined length
     * @param $value
     * @param $fieldname
     * @param $attributes
     * @return bool
     */
    public function min_length($value, $fieldname, $attributes)
    {
        if (preg_match("/[^0-9]/", $attributes))
        {
            return false;
        }

        if (function_exists('mb_strlen'))
        {
            $valid = (mb_strlen($value) < $attributes) ? false : true;
        }
        $valid = (strlen($value) < $attributes) ? false : true;
        if ($valid == false) $this->errors[] = "The " . ucwords($fieldname) . " field should be minimum of " . $attributes . " characters in length";
        return $valid;
    }

    /**
     * returns false if value of field exceeds defined length
     * @param $value
     * @param $fieldname
     * @param $attributes
     * @return bool
     */
    public function max_length($value, $fieldname, $attributes)
    {
        if (preg_match("/[^0-9]/", $attributes))
        {
            return false;
        }

        if (function_exists('mb_strlen'))
        {
            $valid = (mb_strlen($value) > $attributes) ? false : true;
        }

        $valid = (strlen($value) > $attributes) ? false : true;
        if ($valid == false) $this->errors[] = "The " . ucwords($fieldname) . " field should be maximum of " . $attributes . " characters in length";
        return $valid;
    }

    /**
     * returns false if value of field is not of exact defined length
     * @param $value
     * @param $fieldname
     * @param $attributes
     * @return bool
     */
    public function exact_length($value, $fieldname, $attributes)
    {
        if (preg_match("/[^0-9]/", $attributes))
        {
            return false;
        }

        if (function_exists('mb_strlen'))
        {
            $valid = (mb_strlen($value) != $attributes) ? false : true;
        }

        $valid = (strlen($value) != $attributes) ? false : true;
        if ($valid == false) $this->errors[] = "The " . ucwords($fieldname) . " field should be exact of " . $attributes . " characters in length";
        return $valid;
    }
}