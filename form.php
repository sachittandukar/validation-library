<?php
/**
 * Author: Sachit Tandukar, Dipika Shrestha
 * Email: linkin_sachit@hotmail.com
 * Website: http://twitter.com/sachitandukar
 * File: form.php
 */
//include validation class
require 'validation.php';
$validation = new Validation();
//set rules for your form
$rules = array(
    'email' => 'email|required',
    'name' => 'required|min_length:5',
    'password' => 'required',
    'password_confirmation' => 'required|matches:password'
);
//set error delimiters (html element you want to wrap error messages)
$validation->set_error_delimiters('<p class="error">','</p>');
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Validation Library</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/vendor/custom.modernizr.js"></script>
</head>
<body>
<div class="row">
    <div class="large-12 columns">
        <div class="large-6 columns large-centered">
            <?php
            if($_POST):
                //if validation is false show validation error messages
                if($validation->validate($_POST,$rules)==false):
                    echo $validation->validation_error();
                else:
                    echo '<p class="form-success">Form was submitted successfully</p>';
                endif;
            endif;
            ?>
        </div>
    </div>
    <div class="large-12 columns">
        <form action="#" method="post" class="large-6 columns large-centered">
            <ul>
                <li><label for="name">Name:</label><input type="text" name="name" id="name" placeholder="Name"/></li>
                <li><label for="email">Email:</label><input type="text" name="email" id="email" placeholder="Your E-mail Address"/></li>
                <li><label for="password">Password:</label><input type="password" name="password" id="password" placeholder="Password"/></li>
                <li><label for="password-confirmation">Password Confirmation:</label><input type="password" name="password_confirmation" id="password-confirmation" placeholder="Password Confirmation"/></li>
                <li><input type="submit" name="Submit" value="Submit" class="button success small"/></li>
            </ul>
        </form>
    </div>
</div>

<script>
    document.write('<script src=' +
        ('__proto__' in {} ? 'js/vendor/zepto' : 'js/vendor/jquery') +
        '.js><\/script>')
</script>

<script src="js/foundation.min.js"></script>
<!--

<script src="js/foundation/foundation.js"></script>

<script src="js/foundation/foundation.placeholder.js"></script>

<script src="js/foundation/foundation.forms.js"></script>

-->

<script>
    $(document).foundation();
</script>
</body>
</html>
